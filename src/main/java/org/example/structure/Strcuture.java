package org.example.structure;

public class Strcuture {

    static int amountOfMoneyOnAccount = 100;
    static int itemPrice = 200;

    public static void getIfElse(){



        if (amountOfMoneyOnAccount < itemPrice) {
            System.out.println("You don't have enough money on your account to purchase "
                    + "this item.");
        } else {
            // purchase logic implementation here
            System.out.println("Item is purchased");
        }


    }

    public static void getIfElseWithOutBraces(){

        amountOfMoneyOnAccount += 200;
        if (amountOfMoneyOnAccount < itemPrice)
            System.out.println("You don't have enough money on your account to purchase " + "this item.");
        else
            // purchase logic implementation here
            System.out.println("Item is purchased");
        System.out.println("I'm not in else block here");



        int i1 = 3;
        int i2 = 2;
        int i3 = 10;
        if (i1 > i2) {
            if (i1 > i3)
                System.out.println("i1 is more than i2 and i3");
            else
                System.out.println("i1 is more than i2 but less than i3");
        }


    }


    public static void getIfElseIfElse(){
        int orderPrice = 1000;

        if (orderPrice > 0 && orderPrice < 100) {
            System.out.println("You have 5% discount for this order");
        } else if (orderPrice >= 100 && orderPrice < 500) {
            System.out.println("You have 10% discount for this order");
        } else if (orderPrice >= 500 && orderPrice < 5000) {
            System.out.println("You have 15% discount for this order");
        }
    }


    public  static void getSwitch(){
        // expression types available: byte, short, char, int, String, Enum
        String customerStatus = "premium_customer";
        switch (customerStatus) {
            case "guest":
                System.out.println("Thank you for your order!");
                break;
            case "regular_customer":
                System.out.println(
                        "Thank you for your purchase! Take discount 10% for your order "
                                + "as a gratitude for staying with us.");
                break;
            case "premium_customer":
                System.out.println("Wow! You are our PREMIUM customer! Just take all order "
                        + "for 1 USD.");
                break;
            default:
                System.out.println("Customer doesn't have status set.");
        }

        System.out.println("============ No break demo");
        int i = 1;
        switch (i) {
            case 1:
                System.out.println("one");
            case 2:
                System.out.println("two");
                break;
            default:
                System.out.println("This is default block");
        }

        System.out.println("============ Default block demo");

        i = 10;
        switch (i) {
            case 1:
                System.out.println("one");
            case 2:
                System.out.println("two");
                break;
            default:
                System.out.println("This is default block");
        }

    }




    public static void getBreakAndContinue(){

        System.out.println("=========== continue");
        for (int i = 0; i <= 5; i++) {
            if (i % 2 == 0) {
                System.out.println("Dans le if " + i);
                continue;
            }
            System.out.println("counter: " + i);
        }

        System.out.println("=========== break");
        for (int i = 0; i < 5; i++) {
            if (i == 3) {
                break;
            }
            System.out.println("counter: " + i);
        }

        System.out.println("=========== break nested loop");
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    break;
                }
                System.out.print(j + " ");
            }

            System.out.println("counter: " + i);
        }

    }


}

package org.example;

import com.sun.net.httpserver.Filter;
import org.example.String.ChaineCaractere;
import org.example.String.ChaineCaractere1;
import org.example.methode.Method;
import org.example.methode.MethodParam;
import org.example.structure.Strcuture;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Arrays;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        System.out.println("Hello World!");

        // sout  + tab => permet :
        System.out.println();

        // Les  variables :
        //  Variable.getVariable();
        // Variable.getBinary();


        // Les Operators :
        //  Operator.getOperators();
        // Operator.operationsWithIntegersAndDoubles();
        // Operator1.getExpressionType();
        // Operator1.getMathMethod();
        // Operator1.getPrimivitiveAndComparaisonType();

        // Console :

        // ReadFromConcole.getReadWriteConcole();

        // String :
        //   ChaineCaractere.getMethodString();
        // ChaineCaractere.getComparaison();
        //ChaineCaractere.getFormatting();

        // ChaineCaractere.getFormattingWithFormater();
        // ChaineCaractere1.getRegex();

        Strcuture.getBreakAndContinue();


        //Strcuture.getBreakAndContinue();

        /*int i = 10;

        MethodParam.changeIntValue(i);
        System.out.println("valeur de i " + i);
         i = MethodParam.changeInt(i);
        System.out.println("valeur de i " + i);

        int[] array = {1, 2, 3};

        System.out.println("array before changeArray():\t" + Arrays.toString(array));
        MethodParam.changeArray(array);
        System.out.println("array after changeArray():\t" + Arrays.toString(array));

        MethodParam.clearArray(array);
        System.out.println("array after clearArray():\t" + Arrays.toString(array));*/


    }
}

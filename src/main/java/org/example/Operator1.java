package org.example;

import java.util.Arrays;

public class Operator1{
    
    
    public static void getMathMethod(){


        System.out.println(Math.PI);		// 3.141592653589793
        System.out.println(Math.max(3, 5)); // 5
        System.out.println(Math.min(3, 5));	// 3
        System.out.println(Math.sqrt(4));	// 2.0
        int absoluteValue = Math.abs(-5);
        System.out.println(absoluteValue);	// 5

        System.out.println(Math.sqrt(-1));	// NaN
        System.out.println(0 / 0.0);		// NaN
        System.out.println((0 / 0.0) + 5);	// NaN

        System.out.println(5 / 0.0);		// Infinity
        System.out.println(-5 / 0.0);		// -Infinity

        System.out.println(Math.round(20.0 / 3.0));						// 7
        System.out.println(Math.round( 20.0 * 100.0 / 3.0) / 100.0);	// 6.67

        System.out.println(Math.random());						// between 0.0 and 1.0
        System.out.println((int)(Math.random() * 100)); 		// between 0 and 100
        System.out.println((int)(Math.random() * 100) + 100); 	// between 100 and 200
    }
    

    public static void getExpressionType(){

        byte b = 1;
        short s = 1;
        int i = 1;
        long l = 1;

        float f = 1.0F;
        double d = 1.0;

        char c = 1;

        int intExpressionType = b + b;
        int intExpressionType2 = s + s;
        int intExpressionType3 = b + i + c;

        long longExpressionType = i + l;

        float floatExpressionType = l + f;

        double doubleExpressionType = f + d;

        System.out.println(10 / 3); // 3
        System.out.println(10.0 / 3); // 3.3333333333333335

        System.out.println("Hello " + 1); // Hello 1
        System.out.println("Hello " + null); // Hello null




    }
    

    public static void getPrimivitiveAndComparaisonType(){
        int int1 = 128;
        int int2 = 128;

        System.out.println("int1 == int2: " + (int1 == int2));		// true
        System.out.println("1 == 2: " + (1 == 2));					// false
        System.out.println("65 == 'A': " + (65 == 'A'));			// true

        Integer i = 128;
        Integer i2 = 128;

        System.out.println("i == i2: " + (i == i2));				// false

        Integer i3 = 127;
        Integer i4 = 127;
        System.out.println("i3 == i4: " + (i3 == i4));		// true

        Integer i5 = new Integer(127);
        Integer i6 = new Integer(127);
        System.out.println("i5 == i6: " + (i5 == i6));		// false

        Integer i7 = Integer.valueOf(127);
        Integer i8 = Integer.valueOf(127);
        System.out.println("i7 == i8: " + (i7 == i8));		// true

        System.out.println("i.equals(i2): " + i.equals(i2));	// true

        int[] arr1 = {1, 2, 3};
        int[] arr2 = {1, 2, 3};

        System.out.println("arr1 == arr2: " + (arr1 == arr2));			// false
        System.out.println("arr1.equals(arr2): " + arr1.equals(arr2)); 	// false
        System.out.println("Arrays.equals(arr1, arr2): " + Arrays.equals(arr1, arr2));	// true

        arr1 = arr2;
        System.out.println(arr1 == arr2);				// true
    }



}

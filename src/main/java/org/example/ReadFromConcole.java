package org.example;

import java.util.Scanner;

public class ReadFromConcole {

    public static void getReadWriteConcole(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please, enter any word: ");
        String word = sc.next();
        System.out.println("You entered this word: " + word);
        System.out.print("Please, enter any integer number: ");
        int i = sc.nextInt();
        System.out.println("You entered this integer number: " + i);
    }



}

package org.example;

public class Variable {


    public static void getVariable(){

        byte b = 1;
        short s;
        s = 2;
        int i = 3;
        long l = 4;

        char c = 'a';

        boolean bool = true;

        float f = 1.2F;

        long l2 = 2_000_000_000_000L;

        double d = 1.3;

   //     var v = 1;
//		v = true;
//		v = 1.3;

        int i3 = s;

        char c2 = 100;
        System.out.println(c2);

        double d2 = i3;
        d2 = l;

        b = (byte)i3;

        byte b2 = (byte)128;
        System.out.println(b2);

        long number = 499_999_999_000_000_001L;
        double converted = (double) number;
        System.out.println(number - (long) converted);

        Integer i4 = 1;

        int i5 = i4;

    }

    public static void getBinary(){

        int dec = 152;            //  no prefix   --> decimal literal
        int bin = 0b10011000;     // '0b' prefix --> binary literal
        int oct = 0230;           // '0' prefix  --> octal literal
        int hex = 0x98;           // '0x' prefix --> hexadecimal literal

        System.out.println(dec);
        System.out.println(bin);
        System.out.println(oct);
        System.out.println(hex);
    }



}
